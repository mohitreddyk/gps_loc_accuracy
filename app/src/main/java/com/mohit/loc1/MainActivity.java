package com.mohit.loc1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import mad.location.manager.lib.Commons.Utils;
import mad.location.manager.lib.Interfaces.LocationServiceInterface;
import mad.location.manager.lib.Interfaces.SimpleTempCallback;
import mad.location.manager.lib.Services.KalmanLocationService;
import mad.location.manager.lib.Services.ServicesHelper;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    SupportMapFragment mapFragment;
    GoogleMap googleMap;
    Marker marker, marker_kal;
    LocationBroadcastReceiver rec;
    private CheckBox service_checkBox, path_checkBox, kalman_cb, fusion_cb;
    private Boolean service_checkBox_Bool = false, path_checkBox_bool = false , kalman_cb_bool = false , fusion_cb_bool = false;
    Polyline gpsTrack, kalmanTrack, fusionTrack;
    private Kalman kalman;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        service_checkBox = findViewById(R.id.service_checkBox);
        path_checkBox = findViewById(R.id.path_checkBox);
        kalman_cb = findViewById(R.id.kalaman_cb);
        fusion_cb = findViewById(R.id.fusion_cb);
        kalman = new Kalman(this);



        service_checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (service_checkBox.isChecked()) {

                    service_checkBox_Bool = true;
                    Toast.makeText(MainActivity.this, "Location Service Started", Toast.LENGTH_SHORT).show();
                } else {
                    service_checkBox_Bool = false;
                    Toast.makeText(MainActivity.this, "Location Service Stopped", Toast.LENGTH_SHORT).show();
                }

            }
        });


        path_checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (path_checkBox.isChecked()){
                    path_checkBox_bool = true;
                    Toast.makeText(MainActivity.this, "Path History ON", Toast.LENGTH_SHORT).show();
                } else {
                    path_checkBox_bool = false;
                    Toast.makeText(MainActivity.this, "Path History OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });

        kalman_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (kalman_cb.isChecked()){
                    kalman_cb_bool = true;
                    Toast.makeText(MainActivity.this, " Kalman Path History ON", Toast.LENGTH_SHORT).show();
                } else {
                    kalman_cb_bool = false;
                    Toast.makeText(MainActivity.this, "Kalman Path History OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });

        fusion_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fusion_cb.isChecked()){
                    fusion_cb_bool = true;
                    Toast.makeText(MainActivity.this, "Sensor Fusion Path History ON", Toast.LENGTH_SHORT).show();
                } else {
                    fusion_cb_bool = false;
                    Toast.makeText(MainActivity.this, "Sensor Fusion Path History OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //req loc perm
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {
                // start loc ser

                    startService();

            }

        } else {
            // start loc ser
            startService();
        }

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFrag);
        mapFragment.getMapAsync(this); // this loads map
    }

    void startService() {
        rec = new LocationBroadcastReceiver();
        registerReceiver(rec, new IntentFilter("Action_Loc"));
        Intent intent = new Intent(MainActivity.this, LocationService.class);
        startService(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startService();
                } else {
                    Toast.makeText(this, "give me perm mate", Toast.LENGTH_LONG).show();

                }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (this.googleMap == null)
            this.googleMap = googleMap;

        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.color(Color.RED);
        polylineOptions.width(3);
        gpsTrack = googleMap.addPolyline(polylineOptions);

        PolylineOptions polylineOptions2 = new PolylineOptions();
        polylineOptions2.color(Color.GREEN);
        polylineOptions2.width(3);
        kalmanTrack = googleMap.addPolyline(polylineOptions2);

        PolylineOptions polylineOptions3 = new PolylineOptions();
        polylineOptions3.color(Color.BLUE);
        polylineOptions3.width(3);
        fusionTrack = googleMap.addPolyline(polylineOptions3);

    }

    // this listens to the broadcast, the intent is how you get it
    public class LocationBroadcastReceiver extends BroadcastReceiver {

        private long timeStamp; // millis
        private double latitude; // degree
        private double longitude; // degree
        private float variance; // P matrix. Initial estimate of error

        public void setState(double latitude, double longitude, long timeStamp, float accuracy) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.timeStamp = timeStamp;
            this.variance = accuracy * accuracy;
        }

        public void process(float newSpeed, double newLatitude, double newLongitude, long newTimeStamp, float newAccuracy) {
            // Uncomment this, if you are receiving accuracy from your gps
            // if (newAccuracy < Constants.MIN_ACCURACY) {
            //      newAccuracy = Constants.MIN_ACCURACY;
            // }
            if (variance < 0) {
                // if variance < 0, object is unitialised, so initialise with current values
                setState(newLatitude, newLongitude, newTimeStamp, newAccuracy);
            } else {
                // else apply Kalman filter
                long duration = newTimeStamp - this.timeStamp;
                if (duration > 0) {
                    // time has moved on, so the uncertainty in the current position increases
                    variance += duration * newSpeed * newSpeed / 1000;
                    timeStamp = newTimeStamp;
                }

                // Kalman gain matrix 'k' = Covariance * Inverse(Covariance + MeasurementVariance)
                // because 'k' is dimensionless,
                // it doesn't matter that variance has different units to latitude and longitude
                float k = variance / (variance + newAccuracy * newAccuracy);
                // apply 'k'
                latitude += k * (newLatitude - latitude);
                longitude += k * (newLongitude - longitude);
                // new Covariance matrix is (IdentityMatrix - k) * Covariance
                variance = (1 - k) * variance;


            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("Action_Loc") && service_checkBox_Bool) {
                double lat = intent.getDoubleExtra("lat", 0f);
                double lng = intent.getDoubleExtra("lng", 0f);
                float acc = intent.getFloatExtra("acc", 0f);
                float vel = intent.getFloatExtra("vel", 0f);
                long time = intent.getLongExtra("time", 0);
                //Toast.makeText(MainActivity.this, "Lat:" + lat + " ,Lng:" + lng, Toast.LENGTH_SHORT).show();

                process(vel,lat,lng,time,acc);

                if (googleMap != null) {
                    LatLng latLng = new LatLng(lat, lng);
                    LatLng latLng_kal = new LatLng(latitude,longitude);
                    if (marker != null) {
                        marker.setPosition(latLng);
                    } else {
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        marker = googleMap.addMarker(markerOptions);
                    }

                    // set camera zoom lvl here
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 19f));
                    //googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                    if (kalman_cb_bool){
                        List<LatLng> points = kalmanTrack.getPoints();
                        points.add(latLng_kal);
                        kalmanTrack.setPoints(points);
                    }

                    if (path_checkBox_bool) {
                        List<LatLng> points = gpsTrack.getPoints();
                        points.add(latLng);
                        gpsTrack.setPoints(points);
                    }
                }

            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        kalman.start_k();
    }

    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // ++++++++++++++++++++++++++ KALMAN CLASS implementation +++++++++++++++++++++++++++++++++
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    // Kalman variables and things
    public static double ACCELEROMETER_DEFAULT_DEVIATION = 0.1;
    public static final int SENSOR_POSITION_MIN_TIME = 500;
    public static final int GPS_MIN_TIME = 20;
    public static final int GPS_MIN_DISTANCE = 0;
    public static final int SENSOR_DEFAULT_FREQ_HZ = 10;
    public static final int GEOHASH_DEFAULT_PREC = 8;
    public static final int GEOHASH_DEFAULT_MIN_POINT_COUNT = 3;
    public static final double DEFAULT_VEL_FACTOR = 1.0;
    public static final double DEFAULT_POS_FACTOR = 1.0;
    double lat_kal, lng_kal;


    class Kalman implements LocationServiceInterface {
        private Context m_context;
        private KalmanLocationService.Settings settings;

        public Kalman (Context context) {
            m_context = context;
            settings = new KalmanLocationService.Settings(
                    ACCELEROMETER_DEFAULT_DEVIATION,
                    GPS_MIN_DISTANCE,
                    GPS_MIN_TIME,
                    GEOHASH_DEFAULT_PREC,
                    GEOHASH_DEFAULT_MIN_POINT_COUNT,
                    SENSOR_DEFAULT_FREQ_HZ,
                    null,
                    false,
                    DEFAULT_VEL_FACTOR,
                    DEFAULT_POS_FACTOR);

            ServicesHelper.addLocationServiceInterface(this);
        }

        public void start_k () {

            ServicesHelper.getLocationService(m_context, new SimpleTempCallback<KalmanLocationService>() {
                @Override
                public void onCall(KalmanLocationService value) {
                    if (value.IsRunning()) {
                        return;
                    }

                    value.stop();
                    value.reset(settings); //warning!! here you can adjust your filter behavior

                    value.start();
                }
            });
        }


        public void stop_k () {

            ServicesHelper.getLocationService(m_context, new SimpleTempCallback<KalmanLocationService>() {
                @Override
                public void onCall(KalmanLocationService value) {
                    value.stop();
                }
            });
        }

        @Override
        public void locationChanged(Location location) {
            lat_kal = location.getLatitude();
            lng_kal = location.getLongitude();
            Log.d("Kalman output","Long: "+location.getLongitude()+" ,Lat: "+location.getLatitude());
            //Toast.makeText(MainActivity.this, "MAD Kalman Lat:" + lat_kal + " ,Lng:" + lng_kal, Toast.LENGTH_SHORT).show();

            if (googleMap != null) {
                LatLng latlng_fus = new LatLng(lat_kal, lng_kal);

                if (fusion_cb_bool) {
                    List<LatLng> points = fusionTrack.getPoints();
                    points.add(latlng_fus);
                    fusionTrack.setPoints(points);
                }
            }

        }
    }

}


package com.mohit.loc1;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;


public class LocationService extends Service {
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationCallback locationCallback;

    //@Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // log it to the Log cat console
                Log.d("latlngtag", "Lat:" + locationResult.getLastLocation().getLatitude() + ", Lng:" + locationResult.getLastLocation().getLongitude());
                // Broadcast the service
                Intent loc_intent = new Intent("Action_Loc");
                loc_intent.putExtra("lat", locationResult.getLastLocation().getLatitude());
                loc_intent.putExtra("lng", locationResult.getLastLocation().getLongitude());
                loc_intent.putExtra("acc" , locationResult.getLastLocation().getAccuracy());
                loc_intent.putExtra("vel",locationResult.getLastLocation().getSpeed());
                loc_intent.putExtra("time", locationResult.getLastLocation().getTime());
                sendBroadcast(loc_intent);

            }
        };
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        requestLocation();
        return super.onStartCommand(intent, flags, startId);
    }

    @SuppressLint("MissingPermission")
    private void requestLocation(){
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(20);
        locationRequest.setFastestInterval(10);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }
}
